'''
Vidible integration with VLS

authors: Mike Ory, Tom Quinn, Jason Skrifvars

version: 2015.12.08

- Checks upLynk for new assets and links assets to Vidible media objects
'''

import sys
import urllib
import urllib2
import zlib
import hmac
import hashlib
import time
import json
from collections import OrderedDict
import ConfigParser


# this section is required to allow proper authentication when making API calls to both the MPX and upLynk platforms
config = ConfigParser.RawConfigParser()
config.read('defaults.cfg')

# Vidible account info
VIDIBLE_API_URL = 'http://videoimportapi.vidible.tv/importFromUrl'
VIDIBLE_FEED_ID = config.get('vidible', 'feed_id')
VIDIBLE_COMPANY_ID = config.get('vidible', 'company_id')


# UPLYNK account info
UPLYNK_OWNER = config.get('uplynk', 'owner')
UPLYNK_SECRET = config.get('uplynk', 'secret')
UPLYNK_ROOT_URL = 'http://services.uplynk.com/api2'


THUMBNAIL_ASSETTYPE = 'Thumbnail'

def log(*args):
    try:
        line =  time.strftime('%y-%m-%d %H:%M:%S  ') + ' '.join(str(x) for x in args) + '\n'
        sys.stderr.write(line)
        sys.stderr.flush()
    except:
        print 'log:', repr(args)



def notify_vidible(id):
    '''
    run when new asset arrives in upLynk CMS
    '''

    log('*** GETASSET RESPONSE FROM UPLYNK ***')
    asset = call_uplynk('/asset/get', id=id).get('asset')
    
    log(asset)
    #if the asset has no extID make it the id
    if not asset['external_id']:
        asset['external_id'] = asset.get('id')
        #tell uplynk about the new "external_id"
        call_uplynk('/asset/update', id=id, external_id=asset['external_id'])
        log("*** adding external id to uplynk cms ***")
    extid = asset['external_id']
    #asset = get_asset_by_extid(extid)
    log(get_uplynk_thumbs(asset))
    create_vidible_media(asset)
    log('*** DONE PROCESSING {0}'.format(extid))


def get_asset_by_extid(extid):
    retries = 6 # actual number of retries is retries - 1, so 5 here, you can change it if you want to increase retries
    backoff_times = [2**c -1 for c in range(retries)]
    for index, backoff_time in enumerate(backoff_times):
        # backoff for backoff_time seconds
        time.sleep(backoff_time)
        asset = call_uplynk('/asset/get', external_id=extid).get('asset')
        if asset is None and index < retries - 1:
            print('Failed retrieving asset {0} time(s).. Retrying in {1} seconds'.format(index+1, backoff_times[index+1]))
        else:
            break

    log('*** GET ASSET RESPONSE FROM UPLYNK ***')
    log(asset)
    return asset


def get_assetinfo_by_assetid(asset_id):

    assetinfourl = "http://content.uplynk.com/player/assetinfo/{0}.json".format(asset_id)
    #  call asset info API
    req = urllib2.Request(assetinfourl)
    data = json.load(urllib2.urlopen(req))
    log('*** GET ASSET INFO FROM UPLYNK ***')
    log(data)
    return data


def build_vidible_request(request_url):
    '''
    build the web request and append the token for a vidible API call
    :param request_url: url of the vidible service you will be calling
    :return: skeleton web request with auth token
    '''
    request = urllib2.Request(request_url)
    request.add_header('Content-Type', 'application/json')
    return request


def create_vidible_media(asset):
    '''
    creates a Media Object in Vidible
    :param asset: uplynk asset
    :return:
    '''
    log('*** SENDING importFromURL REQUEST TO Vidible ***')
    request = build_vidible_request(VIDIBLE_API_URL)
    extid = asset.get('external_id')
    title = asset.get('desc')
    duration = asset.get('duration')
    uplynkAssetId = asset.get('id')
    upLynkVideoURL = 'http://content.uplynk.com/{0}.m3u8'.format(uplynkAssetId)
    thumblist = get_uplynk_thumbs(asset)
    body = dict(
        {
            "companyId": VIDIBLE_COMPANY_ID,
            "feedId": VIDIBLE_FEED_ID,
            "description": title,
            "length:": duration,
            "name": title,
            "encodedVariants": [
                {
                  "width": 1280,
                  "height": 720,
                  "advancedQuality": False,
                  "audioQuality": 5,
                  "videoQuality": 3,
                  "audioBitRate": 96,
                  "videoBitRate": 1500,
                  "crop": "false",
                  "frameStep": True,
                  "mono": True,
                  "provider": "ZENCODER",
                  "aspectRatio": 0,
                  "videoUrl": upLynkVideoURL,
                  "fileSizeBytes": 8045037,
                  "file": ""
                }
            ],
            "originalVideoUrl": upLynkVideoURL,
            "thumbnails": thumblist,
            "vlsId":uplynkAssetId
        })

    request.data = json.dumps(OrderedDict(sorted(body.items(), key=lambda t: t[0])))
    log(request.data)
    log('*** RESPONSE FROM Vidible ***')
    resp = json.loads(urllib2.urlopen(request).read())
    log(resp)
    return resp


def get_uplynk_thumbs(asset):
    '''
    Builds the uplynk thumbnail list for this asset
    :param asset:
    :return:
    '''
    log('*** GENERATING SMALL & LARGE THUMBNAIL ***')
    asset_id = asset.get('id')
    asset_info = get_assetinfo_by_assetid(asset_id)
    thumb_prefix = asset_info.get('thumb_prefix')
    max_slice = asset_info.get('max_slice')

    thumbs = []

    for n in range(0, max_slice + 1):
        if n == 1:
            break
        str(n)
        filename_small = thumb_prefix + '' + "{0:08x}".format(n) + '.jpg'

        small = {
                  "url": filename_small,
                  "width": 300,
                  "height": 250,
                  "original": False,
                  "covering": True,
                  "type": "AUTO",
                  "version": 1
                }


        log('*** Add Small Thumb to list ***')
        thumbs.append(small)

        filename_large = thumb_prefix + 'upl256' + "{0:08x}".format(n) + '.jpg'
        large = {
                  "url": filename_large,
                  "width": 1280,
                  "height": 720,
                  "original": False,
                  "covering": True,
                  "type": "AUTO",
                  "version": 1
                }

        log('*** Add Large Thumb to list ***')
        thumbs.append(large)

    return thumbs


def call_uplynk(uri, **msg):
    '''
    base method for uplynk API calls
    :return: uplynk API response
    '''
    msg['_owner'] = UPLYNK_OWNER
    msg['_timestamp'] = int(time.time())
    msg = json.dumps(msg)
    msg = zlib.compress(msg, 9).encode('base64').strip()
    sig = hmac.new(UPLYNK_SECRET, msg, hashlib.sha256).hexdigest()
    body = urllib.urlencode(dict(msg=msg, sig=sig))
    resp = json.loads(urllib2.urlopen(UPLYNK_ROOT_URL + uri, body).read())
    return resp


def update_uplynk_meta(id):
    result = call_uplynk('/asset/update', id=id, meta='{"vidible":true}', require_drm=0)
    return result['error']


def get_uplynk_updates(sec):
    epoch = (time.time() - sec) * 1000
    return call_uplynk('/asset/changes', start = epoch)


# get all assets that have been updated in last 15 seconds
def poll():
    while True:
        log("checking...")
        updates = get_uplynk_updates(15)
        
        try:
            assets = updates['assets']
        except:
            print "*** Error.  Do you have your credentials filled in defaults.cfg ? ***"
            print "***"
            raise
        # make sure they have state:ready (so we know they aren't still processing)
        # make sure they dont have meta: {vidible:true}, so we know they haven't been processed
        for id in assets:
            if assets[id]['state'] == 'ready' and \
                    (not 'meta' in assets[id] or not 'vidible' in assets[id]['meta']):
                log("found new asset: " + id)
                notify_vidible(id)
                log("notified vidible")
                log("updating meta for id: " + id)
                log("result of meta update.  error: " + str(update_uplynk_meta(id)))

        time.sleep(15)

if __name__ == '__main__':
    poll()